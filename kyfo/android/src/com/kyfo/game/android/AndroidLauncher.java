package com.kyfo.game.android;

import com.appszoom.appszoomsdk.AppsZoom;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.*;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.example.games.basegameutils.GameHelper;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.kyfo.game.IAction;
import com.kyfo.game.IAnalytics;
import com.kyfo.game.IConnection;
import com.kyfo.game.IPub;
import com.kyfo.game.Kyfo;
import com.kyfo.game.SaveStat;
import com.kyfo.game.ShowToast;
import com.kyfo.game.Statistic;
import com.kyfo.game.RequestManager;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class AndroidLauncher extends AndroidApplication implements GameHelper.GameHelperListener {
    public static final String PREFS_NAME = "MyPrefsFile";

    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */
    String SENDER_ID;
    static final String TAG = "AndroidLauncherLog";
    GoogleCloudMessaging gcm;
    Context context;
    String Registration_ID;
    GooglePlayService googlePlayService;
    private GameHelper gameHelper;
    boolean                 time60 = true, time120 = true, five = true;
    Activity                 activity = this;
    Handler uiThread;
    IConnection             share = null;
    Context appContext;
    Statistic               statistic;
    private RequestManager  requestManager;
    private InterstitialAd  interstitial;
    Tracker                 tracker;
    HashMap<String, String> hitParameters = new HashMap<String, String>();
    private String          follow_id;
    private String          interstitial_id;

    @Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        requestManager = RequestManager.instance();
        statistic = Statistic.instance();
        AppsZoom.start(this);

        Resources res = getResources();
        follow_id = res.getString(R.string.follow_id);
        interstitial_id = res.getString(R.string.interstitial_id);
        SENDER_ID = res.getString(R.string.app_id);
        requestManager.url = res.getString(R.string.url);

        SharedPreferences       sharedPref;
        sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        statistic.pk = sharedPref.getString(getString(R.string.pk), "pk");
        statistic.login = sharedPref.getString(getString(R.string.login), "login");
        statistic.pub = sharedPref.getBoolean(getString(R.string.pub), true);
        statistic.nbShare = sharedPref.getInt(getString(R.string.nbPartage), 0);
        statistic.shared = sharedPref.getBoolean(getString(R.string.shared), false);
        statistic.maxTotal = sharedPref.getInt(getString(R.string.best), 0);
        statistic.New = sharedPref.getBoolean(getString(R.string.New), true);
        googlePlayService = GooglePlayService.instance();

        uiThread = new Handler();
        this.appContext = getApplicationContext();
        setContentView(R.layout.main);
        context = getApplicationContext();


        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        GoogleAnalytics.getInstance(this).setLocalDispatchPeriod(15);
        tracker = analytics.newTracker(follow_id);
        // Créez l'interstitiel.
        interstitial = new InterstitialAd(this);
        interstitial.setAdUnitId(interstitial_id);
        requestNewInterstitial();
        // Créez la demande d'annonce.


        //new ConnecationApiGoogle().execute();
        startGame();
            gameHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
            gameHelper.enableDebugLog(true);
        gameHelper.setup(this);
        statistic.email = getAccountName();
        loadScoreOfLeaderBoard();
	}

    private String getAccountName() {

        String accountName = null;

        AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
        Account[] list = manager.getAccounts();
        for (Account account : list) {
            if (account.type.equalsIgnoreCase("com.google")) {
                accountName = account.name;
                break;
            }
        }
        return accountName;
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        interstitial.loadAd(adRequest);
    }

    private void loadScoreOfLeaderBoard() {
        if (!gameHelper.isConnecting())
            return;
        Games.Leaderboards.loadCurrentPlayerLeaderboardScore(gameHelper.getApiClient(), googlePlayService.LEADERBOARD_BEST, LeaderboardVariant.TIME_SPAN_ALL_TIME, LeaderboardVariant.COLLECTION_PUBLIC).setResultCallback(new ResultCallback<Leaderboards.LoadPlayerScoreResult>() {
            @Override
            public void onResult(final Leaderboards.LoadPlayerScoreResult scoreResult) {
                if (isScoreResultValid(scoreResult)) {
                    // here you can get the score like this
                    int time = ((int) scoreResult.getScore().getRawScore()) / 1000;
                    statistic.bestTime = time < statistic.bestTime ? statistic.bestTime : time;
                    if (statistic.bestTime >= 60) {
                        Games.Achievements.unlock(gameHelper.getApiClient(), googlePlayService.ACHIEVEMENT_ONE_MINUTE);
                    }
                    if (statistic.bestTime >= 120) {
                        Games.Achievements.unlock(gameHelper.getApiClient(), googlePlayService.ACHIEVEMENT_TWO_MINUTE);
                    }
//                    statistic.maxTotal = score > statistic.maxTotal ? score : statistic.maxTotal;
//                    if (score < statistic.maxTotal) {
//                        if (gameHelper.isSignedIn()) {
//                            Games.Leaderboards.submitScore(gameHelper.getApiClient(), googlePlayService.LEADERBOARD_ID, statistic.maxTotal);
//                        }
//                    }
                }
            }
        });
    }

    private boolean isScoreResultValid(final Leaderboards.LoadPlayerScoreResult scoreResult) {
        return scoreResult != null && GamesStatusCodes.STATUS_OK == scoreResult.getStatus().getStatusCode() && scoreResult.getScore() != null;
    }

    public void startGame() {
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        initialize(new Kyfo(new IAnalytics() {
            @Override
            public void screenName(String screenName) {
                tracker.setScreenName(screenName);
                tracker.send(new HitBuilders.AppViewBuilder().build());
            }

            @Override
            public void event(String categoryId, String actionId, String labelId) {
                tracker.send(new HitBuilders.EventBuilder()
                        .setCategory(categoryId)
                        .setAction(actionId)
                        .setLabel(labelId)
                        .build());
            }
        }, new SaveStat() {
            @Override
            public void save() {
                SharedPreferences       sharedPref;
                sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putFloat(getString(R.string.bestTime), statistic.bestTime);
                editor.putInt(getString(R.string.best), statistic.maxTotal);
                editor.putInt(getString(R.string.money), statistic.money);
                editor.putString(getString(R.string.pk), statistic.pk);
                editor.putBoolean(getString(R.string.shared), statistic.shared);
                editor.putString(getString(R.string.login), statistic.login);
                editor.putBoolean(getString(R.string.pub), statistic.pub);
                editor.putBoolean(getString(R.string.New), false);
                editor.putBoolean(getString(R.string.life), statistic.life);
                editor.putBoolean(getString(R.string.soufle), statistic.soufle);
                editor.putBoolean(getString(R.string.gravity), statistic.gravity);
                editor.putBoolean(getString(R.string.lvl), statistic.mult);
                editor.putBoolean(getString(R.string.clear), statistic.clear);
                editor.putInt(getString(R.string.nbPartage), statistic.nbShare);
                editor.commit();
            }
        }, new IPub() {
            @Override
            public void showInterstitial() {
                handler.sendEmptyMessage(SHOW_INTERSTITIAL);
            }

            public void showAdView() {
                handler.sendEmptyMessage(SHOW_ADVIEW);
            }



            @Override
            public void displayAchievement() {
//                if (!gameHelper.isConnecting())
//                    return ;
                if (gameHelper.isSignedIn() && isNetworkConnected()) {
                    startActivityForResult(Games.Achievements.getAchievementsIntent(gameHelper.getApiClient()), googlePlayService.REQUEST_ACHIEVEMENTS);
                } else {
                    toast("Achievement is not available", true);
                }

            }

            @Override
            public void unlockTime60() {
                if (!gameHelper.isSignedIn())
                    return ;
                if (time60) {
                    time60 = false;
                    Games.Achievements.unlock(gameHelper.getApiClient(), googlePlayService.ACHIEVEMENT_ONE_MINUTE);
                }
            }

            @Override
            public void unlockFive() {
                if (!gameHelper.isSignedIn())
                    return ;
                if (five) {
                    five = false;
                    Games.Achievements.unlock(gameHelper.getApiClient(), googlePlayService.ACHIEVEMENT_FIVE);
                }
            }

            @Override
            public void unlockTime120() {
                if (!gameHelper.isSignedIn())
                    return ;
                if (time120) {
                    time120 = false;
                    Games.Achievements.unlock(gameHelper.getApiClient(), googlePlayService.ACHIEVEMENT_TWO_MINUTE);
                }
            }

            @Override
            public void firstTime() {
                if (!gameHelper.isSignedIn())
                    return ;
                if (gameHelper.isSignedIn())
                    Games.Achievements.unlock(gameHelper.getApiClient(), googlePlayService.ACHIEVEMENT_FIRST_TIME);
            }

            @Override
            public boolean isNetworkConnected() {
                ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo ni = cm.getActiveNetworkInfo();
                if (ni == null) {
                    // There are no active networks.
                    return false;
                } else
                    return true;
            }

            @Override
            public void displayScore() {

                if (gameHelper.isSignedIn() && isNetworkConnected()) {
                    startActivityForResult(Games.Leaderboards.getLeaderboardIntent(gameHelper.getApiClient(), googlePlayService.LEADERBOARD_BEST), googlePlayService.REQUEST_LEADERBOARD);
                } else {
                    toast("Rank is not available", true);
                }
            }

            @Override
            public void submitScore() {
                if (gameHelper.isSignedIn()) {
                    Games.Leaderboards.submitScore(gameHelper.getApiClient(), googlePlayService.LEADERBOARD_BEST, statistic.maxTotal);
                    if (statistic.nbPlay >= 10)
                        Games.Achievements.unlock(gameHelper.getApiClient(), googlePlayService.ACHIEVEMENT_BORED);
                    if (statistic.nbPlay >= 100)
                        Games.Achievements.unlock(gameHelper.getApiClient(), googlePlayService.ACHIEVEMENT_REALY_REALY_BORED);
                }
            }

            @Override
            public void dialogueShare(IConnection action) {
                DialogueShare dialogueShare = new DialogueShare();
                dialogueShare.action = action;
                dialogueShare.show(activity.getFragmentManager(), "Share");
            }


            @Override
            public void dialogueInscription(IConnection action) {
                DialogInscription dialogInscription = new DialogInscription();
                dialogInscription.action = action;
                dialogInscription.show(activity.getFragmentManager(), "Inscription");
            }

            @Override
            public void scan(IConnection s) {
                share = s;
                handler.sendEmptyMessage(SHOW_SCAN);
            }

            @Override
            public void getRegId(IAction action) {
                if (checkPlayServices()) {
                    gcm = GoogleCloudMessaging.getInstance(activity);
                    Registration_ID = getRegistrationId(context);
                    Log.i(TAG,  "regid = " + Registration_ID);
                    //Toast.makeText(appContext, "reg_id = " + Registration_ID, Toast.LENGTH_SHORT)
                    //        .show();
                    if (Registration_ID.equals("")) {
                        registerInBackground(action);
                    } else {
                        statistic.registration_ID = Registration_ID;
                        action.exec();
                    }
                } else {
                    Log.i(TAG, "No valid Google Play Services APK found.");
                }
            }


            @Override
            public void generateQRCode(IConnection action) {
                try {
                    // generate a 150x150 QR code

                    Bitmap bm = encodeAsBitmap(requestManager.url + "scan?email=" + statistic.email, BarcodeFormat.QR_CODE, statistic.UNITY * 8, statistic.UNITY * 8);

                    if(bm != null) {
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        action.generate(byteArray);
//                image_view.setImageBitmap(bm);
                    }
                } catch (WriterException e) { //eek }
                }
            }

            @Override
            public void dialogueLogin(IConnection action) {
                DialogLogin login = new DialogLogin();
                login.action = action;
                login.show(activity.getFragmentManager(), "Connection");
            }
        }, new ShowToast() {
            @Override
            public void show(String message, boolean isShort) {
                toast(message, isShort);
            }
        }), config);

    }

    public void displayInterstitial() {
        if (interstitial.isLoaded()) {
            interstitial.show();
        } else {
//            finish();
        }

//        switch (MathUtils.random(0, 1)) {
//            case 0:
//                AppsZoom.showAd(this);
//                break;
//            default:
//                if (interstitial.isLoaded()) {
//                    interstitial.show();
//                } else {
////            finish();
//                }
//        }
    }
    private final int SHOW_SCAN = 2;
    private final int SHOW_ADVIEW = 1;
    private final int SHOW_INTERSTITIAL = 0;

    protected Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                case SHOW_INTERSTITIAL:
                {
                    displayInterstitial();
                    break;
                }
                case SHOW_ADVIEW:
                {
                    //displayAdView();
                    break;
                }
                case SHOW_SCAN:
                {
                    IntentIntegrator scanIntegrator = new IntentIntegrator(activity);
                    scanIntegrator.initiateScan();
                }
            }
        }
    };


    public void toast(final String message, final boolean isShort) {
        uiThread.post(new Runnable() {
            public void run() {
                if (isShort) {
                    Toast.makeText(appContext, message, Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Toast.makeText(appContext, message, Toast.LENGTH_LONG)
                            .show();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        //
        interstitial.loadAd(adRequest);
        checkPlayServices();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(AndroidLauncher.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        Log.i(TAG, "getAppVersion");
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void registerInBackground(IAction action) {
        Log.i(TAG,  "registerInBackground");
       new toto(action).execute();
    }

    private void sendRegistrationIdToBackend() {
        statistic.registration_ID = Registration_ID;
        // Your implementation here.
    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }

    class toto extends AsyncTask<Void, Void, String> {
        IAction action;

        public toto(IAction action) {
            this.action = action;
        }

        @Override
        protected String doInBackground(Void... params) {
            Log.i(TAG,  "doInBackground");
            String msg = "";
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(context);
                }
                Registration_ID = gcm.register(SENDER_ID);
                msg = "Device registered, registration ID=" + Registration_ID;

                // You should send the registration ID to your server over HTTP,
                // so it can use GCM/HTTP or CCS to send messages to your app.
                // The request to your server should be authenticated if your app
                // is using accounts.
                Log.i(TAG,  msg);
 //               Toast.makeText(appContext, msg, Toast.LENGTH_SHORT)
//                        .show();
                sendRegistrationIdToBackend();
                action.exec();
                // For this demo: we don't need to send it because the device
                // will send upstream messages to a server that echo back the
                // message using the 'from' address in the message.

                // Persist the regID - no need to register again.
                storeRegistrationId(context, Registration_ID);
            } catch (IOException ex) {
                msg = "Error :" + ex.getMessage();
                Log.i(TAG,  msg);
                // If there is an error, don't just keep trying to register.
                // Require the user to click a button again, or perform
                // exponential back-off.
            }
            return msg;
        }
    }



    @Override
    public void onStart(){
        super.onStart();
        gameHelper.onStart(this);
    }

    @Override
    public void onStop(){
        super.onStop();
        gameHelper.onStop();


    }
    @Override
    public void onActivityResult(int request, int response, Intent data) {
        super.onActivityResult(request, response, data);
        gameHelper.onActivityResult(request, response, data);
       // statistic.login = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);

        IntentResult scanningResult = IntentIntegrator.parseActivityResult(request, response, data);
        if (scanningResult != null) {
            if (share != null) {
                String strToast = "";
                String[] tab = scanningResult.getContents().split("\\s+");

                if (tab.length == 2 && tab[0].equals("kyfo")) {
                    String mail = tab[1];
                    String scanFormat = scanningResult.getFormatName();
                    strToast = "this QR Code is valid";
                    share.share(mail);
                } else {
                    strToast = "it's not a valid QR Code";
                }

                Toast toast = Toast.makeText(getApplicationContext(),
                        strToast, Toast.LENGTH_SHORT);

                toast.show();
            }
//            String scanContent = scanningResult.getContents();
//            String scanFormat = scanningResult.getFormatName();
//                        Toast toast = Toast.makeText(getApplicationContext(),
//                    scanContent, Toast.LENGTH_SHORT);
//            toast.show();
        } else {

        }
    }

    private static final int GREEN = 0xFF00FF00;
    private static final int RED = 0xFFFF0000;
    private static final int BLACK = 0x00FFFFFF;

    Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : statistic.shared ? RED : GREEN;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }
}
