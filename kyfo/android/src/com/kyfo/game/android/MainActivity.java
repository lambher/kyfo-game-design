//package com.kyfo.game.android;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.FragmentActivity;
//import android.util.Log;
//import android.view.Menu;
//import android.view.MenuItem;
//
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.drive.Drive;
//import com.google.android.gms.games.Games;
//import com.google.android.gms.plus.Plus;
//import com.google.example.games.basegameutils.BaseGameUtils;
//
//
//public class MainActivity extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, MainMenuFragment.Listener
//{
//
//    // Client used to interact with Google APIs
//    String TAG = "MainActivity";
//    MainMenuFragment mMainMenuFragment;
//
//    private GooglePlayService googlePlayService;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        Bundle b = getIntent().getExtras();
//        int value = getIntent().getIntExtra("key", 0);
//        if (value == 1) {
//            if (googlePlayService.isSignedIn()) {
//                    startActivityForResult(Games.Leaderboards.getLeaderboardIntent(googlePlayService.mGoogleApiClient, googlePlayService.LEADERBOARD_ID), googlePlayService.REQUEST_LEADERBOARD);
//                } else {
//                    BaseGameUtils.makeSimpleDialog(this, getString(R.string.achievements_not_available)).show();
//                }
//        } else {
//            googlePlayService = GooglePlayService.instance();
//            googlePlayService.mGoogleApiClient = new GoogleApiClient.Builder(this)
//                    .addConnectionCallbacks(this)
//                    .addOnConnectionFailedListener(this)
//                    .addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN)
//                    .addApi(Games.API).addScope(Games.SCOPE_GAMES)
//                    .build();
//        }
//
//
////        mMainMenuFragment = new MainMenuFragment();
////        mMainMenuFragment.setListener(this);
////        getSupportFragmentManager().beginTransaction().add(R.email.fragment_container,
////                mMainMenuFragment).commit();
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
//        super.onActivityResult(requestCode, resultCode, intent);
//        Log.d(TAG, "onActivityResult(): " + requestCode + " " + resultCode);
//        if (requestCode == googlePlayService.RC_SIGN_IN) {
//            googlePlayService.mSignInClicked = false;
//            googlePlayService.mResolvingConnectionFailure = false;
//            if (resultCode == RESULT_OK) {
//                googlePlayService.mGoogleApiClient.connect();
//            } else {
//                BaseGameUtils.showActivityResultError(this, requestCode, resultCode, R.string.signin_other_error);
//            }
//        }
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        Log.d(TAG, "onStart(): connecting");
//        googlePlayService.mGoogleApiClient.connect();
//    }
//
//    @Override
//    public void onConnectionFailed(ConnectionResult connectionResult) {
//        Log.d(TAG, "onConnectionFailed(): attempting to resolve");
//        if ( googlePlayService.mResolvingConnectionFailure) {
//            Log.d(TAG, "onConnectionFailed(): already resolving");
//            return;
//        }
//
//        if ( googlePlayService.mSignInClicked ||  googlePlayService.mAutoStartSignInFlow) {
//            googlePlayService.mAutoStartSignInFlow = false;
//            googlePlayService.mSignInClicked = false;
//            googlePlayService.mResolvingConnectionFailure = true;
//            if (!BaseGameUtils.resolveConnectionFailure(this, googlePlayService.mGoogleApiClient, connectionResult,
//                    googlePlayService.RC_SIGN_IN, getString(R.string.signin_other_error))) {
//                googlePlayService.mResolvingConnectionFailure = false;
//            }
//        }
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        Log.d(TAG, "onStop(): disconnecting");
//        if (googlePlayService.mGoogleApiClient.isConnected()) {
//            googlePlayService.mGoogleApiClient.disconnect();
//        }
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int email = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (email == R.email.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
//
//    @Override
//    public void onConnected(Bundle bundle) {
//        Log.d(TAG, "onConnected(): ?");
//        Intent intent = new Intent(MainActivity.this, AndroidLauncher.class);
//        startActivity(intent);
//        //finish();
//    }
//
//    @Override
//    public void onConnectionSuspended(int i) {
//
//    }
//
//    @Override
//    public void onStartGameRequested(boolean hardMode) {
//
//    }
//
//    @Override
//    public void onShowAchievementsRequested() {
//
//    }
//
//    @Override
//    public void onShowLeaderboardsRequested() {
//
//    }
//
//    @Override
//    public void onSignInButtonClicked() {
//        Log.d(TAG, "onSignInButtonClicked(): click");
//        googlePlayService.mGoogleApiClient.connect();
//    }
//
//    @Override
//    public void onSignOutButtonClicked() {
//
//    }
//
////    public static MainActivity instance() {
////        return SingletonHolder.instance;
////    }
////
////    private static class SingletonHolder
////    {
////        private final static MainActivity instance = new MainActivity();
////    }
//}
