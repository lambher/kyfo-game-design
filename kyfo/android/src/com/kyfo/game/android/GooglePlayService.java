package com.kyfo.game.android;

import com.google.android.gms.common.api.GoogleApiClient;

public class GooglePlayService {
    static final int RC_RESOLVE = 5000;
    static final int RC_UNUSED = 5001;
    static final int RC_SIGN_IN = 9001;
    static final int REQUEST_ACHIEVEMENTS = 4524;
    static final String LEADERBOARD_BEST = "CgkIhay5l_AaEAIQDw";
    static final String LEADERBOARD_MONEY = "CgkIhay5l_AaEAIQDQ";
    static final String ACHIEVEMENT_ONE_MINUTE = "CgkIhay5l_AaEAIQAw";
    static final String ACHIEVEMENT_TWO_MINUTE = "CgkIhay5l_AaEAIQBA";
    static final String ACHIEVEMENT_FIRST_TIME = "CgkIhay5l_AaEAIQAg";
    static final String ACHIEVEMENT_FIVE = "CgkIhay5l_AaEAIQBQ";
    static final String ACHIEVEMENT_100 = "CgkIhay5l_AaEAIQBg";
    static final String ACHIEVEMENT_500 = "CgkIhay5l_AaEAIQBw";
    static final String ACHIEVEMENT_1000 = "CgkIhay5l_AaEAIQCA";
    static final String ACHIEVEMENT_10000 = "CgkIhay5l_AaEAIQCQ";
    static final String ACHIEVEMENT_100000 = "CgkIhay5l_AaEAIQCg";
    static final String ACHIEVEMENT_BORED = "CgkIhay5l_AaEAIQCw";
    static final String ACHIEVEMENT_REALY_REALY_BORED = "CgkIhay5l_AaEAIQDA";
    static final int REQUEST_LEADERBOARD = 4400;

    GoogleApiClient mGoogleApiClient;


    boolean mSignInClicked = false;
    boolean mResolvingConnectionFailure = false;
    boolean mAutoStartSignInFlow = true;

    private GooglePlayService() {
    }


    public boolean isSignedIn() {
        return (mGoogleApiClient != null && mGoogleApiClient.isConnected());
    }

    public static GooglePlayService instance() {
        return SingletonHolder.instance;
    }

    private static class SingletonHolder
    {
        private final static GooglePlayService instance = new GooglePlayService();
    }
}