package com.kyfo.game.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.kyfo.game.IAction;
import com.kyfo.game.IConnection;

/**
 * Created by lambe_000 on 20/01/2015.
 */
public class DialogLogin extends DialogFragment {
    IConnection action;
    EditText    editTextLogin, editTextPassword;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        final View view = inflater.inflate(R.layout.dialog_login, null);
        builder.setView(view);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                editTextLogin = (EditText) view.findViewById(R.id.login_connect);
                editTextPassword = (EditText) view.findViewById(R.id.password_connect);
                String login = editTextLogin.getText().toString();
                String password = editTextPassword.getText().toString();
                action.connect(login, password);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        return builder.create();
    }
}