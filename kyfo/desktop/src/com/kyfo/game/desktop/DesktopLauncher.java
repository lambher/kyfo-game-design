package com.kyfo.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.kyfo.game.IAction;
import com.kyfo.game.IAnalytics;
import com.kyfo.game.IConnection;
import com.kyfo.game.IPub;
import com.kyfo.game.Kyfo;
import com.kyfo.game.MyGdxGame;
import com.kyfo.game.SaveStat;
import com.kyfo.game.ShowToast;

public class DesktopLauncher {
	public static void main (String[] arg) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "Keep Your Fingers On";
        cfg.height = 1920 / 3;
        cfg.width = 1200 / 3;
		new LwjglApplication(new Kyfo(new IAnalytics() {
            @Override
            public void screenName(String screenName) {

            }

            @Override
            public void event(String categoryId, String actionId, String labelId) {

            }
        }, new SaveStat() {
            @Override
            public void save() {

            }
        }, new IPub() {
            @Override
            public void showInterstitial() {

            }
            public void showAdView() {

            }

            @Override
            public boolean isNetworkConnected() {
                return false;
            }

            @Override
            public void dialogueShare(IConnection ok) {

            }

            @Override
            public void dialogueLogin(IConnection ok) {

            }

            @Override
            public void dialogueInscription(IConnection ok) {

            }

            @Override
            public void submitScore() {

            }

            @Override
            public void displayScore() {

            }

            @Override
            public void unlockTime60() {

            }

            @Override
            public void unlockTime120() {

            }

            @Override
            public void displayAchievement() {

            }

            @Override
            public void firstTime() {

            }

            @Override
            public void unlockFive() {

            }

            @Override
            public void scan(IConnection share) {

            }

            @Override
            public void getRegId(IAction action) {

            }

            @Override
            public void generateQRCode(IConnection action) {

            }
        }, new ShowToast() {
            @Override
            public void show(String message, boolean isShort) {

            }
        }), cfg);
	}
}
