package com.kyfo.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by lambe_000 on 19/05/2015.
 */
class TouchInfo {
    float   valueIncrease = 1;
    float   touchX = 0;
    float   touchY = 0;
    boolean touched = false;
    boolean endTouch = false;
    float   time = 0;
    boolean killed = false;
    float   timeDirection = 0;
    float   timeFire = 999;
    Vector2 positionBefore;
    Vector2 direction;
    float   goTouchX = -1;
    float   goTouchY = -1;
    float   vitesse = 30;
    float   timeKilled = 0;
    float   ticFire = .25f;
    int     height;
    GameManager gameManager = GameManager.instance();
    private TouchInfoManager touchManager;
    int UNITY = Statistic.instance().UNITY;
    float   rayon = UNITY / 8;

    public TouchInfo() {
        touchManager = TouchInfoManager.instance();
        height = Gdx.graphics.getHeight();
        direction = new Vector2(0, 1);
        positionBefore = new Vector2(0, 0);
    }

    public Vector2 position() {
        return new Vector2(touchX, height - touchY);
    }

    public void beginTouch() {
        if (!endTouch) {
            touched = true;
        }
        endTouch = false;
        this.timeFire = 999;
        this.time = 0;
        this.killed = false;
        this.timeKilled = 0;
    }

    public void fire() {
        touchManager = TouchInfoManager.instance();
        TouchInfo touchInfo = touchManager.touchA;
        if (touchManager.touchA.touched && touchManager.touchB.touched) {
            this.timeFire += Gdx.graphics.getDeltaTime();
            if (this.timeFire >= ticFire) {
                SoundManager.instance().getSound("button").play();
                BallManager.instance().add(new Bullet(new Vector2(touchX, height - touchY), touchManager.getDirectionAB(), vitesse));
                this.timeFire = 0;
            }

        } else {
            this.timeFire = 999;
        }
    }

    public void hit() {
        killed = true;
        timeKilled = 0;

    }

    public void plop() {
        touched = false;
        endTouch = false;
    }

    public void render(ShapeRenderer shapeRenderer) {

        if (goTouchX >= 0) {
            touchX +=  (goTouchX - touchX) / 3.f;
        }
        if (goTouchY >= 0) {
            touchY +=  (goTouchY - touchY) / 3.f;
        }

        if (touched) {
            if (killed == false || (int) (timeKilled * 10) % 2 == 1) {
                shapeRenderer.setColor(Color.GREEN);
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.circle(touchX, height - touchY, rayon);
                shapeRenderer.end();
            }
        }

        if (killed) {
            this.timeKilled += Gdx.graphics.getDeltaTime();
            if (timeKilled > 2) {
                killed = false;
            }
        }

        if (endTouch) {
            this.time += Gdx.graphics.getDeltaTime();
            if (this.time > 0.1) {
                this.time = 0;
                plop();
            }
            if (touchManager.nothingPointerTouched() && Statistic.instance().time > 0) {
                BallManager.instance().fuite(new Vector2(Gdx.graphics.getWidth() / 2, height / 2));
                gameManager.endGame();
            }
        }

    }
}