package com.kyfo.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by lambe_000 on 19/05/2015.
 */
class   GravityBall extends Ball {
    public GravityBall(Vector2 position, Vector2 direction, float rayon, float vitesse) {
        super(Color.MAROON, position, direction, rayon, vitesse);
        sprite = new Sprite(TextureManager.instance().getTexture("gravity"));
        sprite.setScale(1 / 312f * rayon);
    }

    public void effect() {
        SoundManager.instance().getSound("gravity").play();
        for (int i = 0; i < ballManager.nbBall(); i++) {
            Ball boule = ballManager.getBall(i);
            boule.cible = null;
            boule.follow = false;
            boule.direction.x = 0;
            boule.direction.y = -1;
        }
        ballManager.remove(this);
    }
}