package com.kyfo.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.input.GestureDetector;


public abstract class MyGdxGame implements Screen {
    protected OrthographicCamera    camera;
    protected Kyfo                  game;
    protected float                 opacity;

    private SpriteBatch             batch;
    private BallManager             ballManager = BallManager.instance();
    private UIManager               uiManager = UIManager.instance();
    private MyFont                  font;
    private ShapeRenderer           shapeRenderer;
    private int                     UNITY = Statistic.instance().UNITY;
    private int                     height, width;
    private InputMultiplexer        inputMultiplexer;
    private GestureDetector         gestureDetector;
    private GameManager             gameManager;
    private TouchInfoManager        touchManager;
    private Statistic               statistic;

    public MyGdxGame(final Kyfo game) {
        gameManager = GameManager.instance();
        gameManager.setGame(game);
        touchManager = TouchInfoManager.instance();
        inputMultiplexer = new InputMultiplexer();
        gestureDetector = new GestureDetector(new KyfoGesture());
        inputMultiplexer.addProcessor(gestureDetector);
        inputMultiplexer.addProcessor(new KyfoInput(game));
        statistic = Statistic.instance();
        this.game = game;
        shapeRenderer = new ShapeRenderer();
        Gdx.gl20.glLineWidth(2);
        height = Gdx.graphics.getHeight();
        width = Gdx.graphics.getWidth();
        camera = new OrthographicCamera(width, height);
        camera.position.set(width / 2, height / 2, 0);
        camera.update();
        opacity = 1;
        System.out.println(width);
        font = Ressources.instance().getFont("font/bat.ttf", UNITY);
        batch = new SpriteBatch();
    }

    public abstract void end();


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

	public void render(float delta) {
        update(delta);

		Gdx.gl.glClearColor(0, 0, 0.19f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        MyBackground.instance().render(batch);
        uiManager.render(camera, font, batch, shapeRenderer, opacity);
        ballManager.render(shapeRenderer, batch);
        opacity = 1;
        touchManager.getTouchCenter().render(shapeRenderer);
    }




    public void update(float delta) {
        uiManager.update();
        if (statistic.time > 60) {
            game.unlockTime60();
        }
        if (statistic.time > 120) {
            game.pub.unlockTime120();
        }
        if (gameManager.begin) {
            gameManager.time += gameManager.game_slaw ? Gdx.graphics.getDeltaTime() / gameManager.time_slaw : Gdx.graphics.getDeltaTime();
            ballManager.update();
            touchManager.getTouchCenter().fire();
            statistic.time += delta;
            gameManager.time += delta;
            statistic.bestTime = statistic.time > statistic.bestTime ? statistic.time : statistic.bestTime;
            if (gameManager.time > 1) {

                gameManager.time = 0;
            }
        }
        if (touchManager.getTouchCenter().touched)
            MyBackground.instance().setX(((touchManager.getTouchCenter().position().x / width) * 2 - 1));
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        uiManager.resume();
    }


}
