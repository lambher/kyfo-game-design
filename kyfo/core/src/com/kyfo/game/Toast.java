package com.kyfo.game;

/**
 * Created by lambe_000 on 10/05/2015.
 */
public class Toast {

    private ShowToast showToast;

    private Toast() {
        showToast = null;
    }

    public void initialise(ShowToast showToast) {
        this.showToast = showToast;
    }

    public void show_short(String message) {

    }

    public void show_long(String message) {
        showToast.show(message, true);
    }

    public static Toast instance() {
        return SingletonHolder.instance;
    }

    private static class SingletonHolder
    {
        private final static Toast instance = new Toast();
    }
}
