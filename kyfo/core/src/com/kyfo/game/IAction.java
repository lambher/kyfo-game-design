package com.kyfo.game;

/**
 * Created by lambe_000 on 24/12/2014.
 */
public interface IAction {
    public void exec();
}
