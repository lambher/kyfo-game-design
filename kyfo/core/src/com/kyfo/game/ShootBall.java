package com.kyfo.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by lambe_000 on 19/05/2015.
 */
class   ShootBall extends Ball {
    public ShootBall(Vector2 position, Vector2 direction, float rayon, float vitesse) {
        super(Color.BLUE, position, direction, rayon, vitesse);
        sprite = new Sprite(TextureManager.instance().getTexture("soufle"));
        sprite.setScale(1 / 312f * rayon);
    }

    public void effect() {
        SoundManager.instance().getSound("soufle").play();

        int nb = 20;
        for (int i = 0; i < nb; i++) {
            Vector2 p = new Vector2(getPosition());
            Vector2 d = new Vector2(0, 1).rotate((360 / nb) * i);

            ballManager.add(new Bullet(p, d, 20));
        }

        ballManager.remove(this);
    }
}