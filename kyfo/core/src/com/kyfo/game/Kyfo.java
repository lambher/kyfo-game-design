package com.kyfo.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpParametersUtils;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import java.util.HashMap;
import java.util.Map;


public class Kyfo extends Game {
    private Home        home;
    private MyGdxGame   myGdxGame;
    private RequestManager requestManager;

    private NoPub       noPub;
    private Statistic   statistic;
    public IPub         pub;
    private Toast       toast;
    private IAnalytics  analytics;
    private Kyfo        kyfo = this;

    public Kyfo(IAnalytics analytics, SaveStat saveStat, IPub pub, ShowToast showToast) {
        requestManager = RequestManager.instance();
        statistic = Statistic.instance();
        toast = Toast.instance();

        statistic.initialise(saveStat);
        toast.initialise(showToast);

        this.pub = pub;
        this.analytics = analytics;
    }

    @Override
    public void create() {
        Gdx.input.setCatchBackKey(true);

        SoundManager.instance().load();
        MusicManager.instance().load();
        TextureManager.instance().load();

        statistic.UNITY = 84 * Gdx.graphics.getWidth() / 1200;
        myGdxGame = new Solo(this);
        home = new Home(this);
        noPub = null;

        if (!pub.isNetworkConnected()) {
            toast.show_short("Network is not connected");
        }
        checkRegId();

        goToHome();
    }

    public void checkRegId() {
        pub.getRegId(new IAction() {

            // statistic.registration_ID has set
            @Override
            public void exec() {
                requestManager.sendRegId();
            }
        });
    }

    public void displayAchievement() {
        pub.displayAchievement();
    }

    public void unlockTime60() {
        pub.unlockTime60();
    }

    public void submiteScore() {
        pub.submitScore();
    }

    public void displayScore() {
        pub.displayScore();
    }


    public void goToMyGdxGame() {
        if (myGdxGame == null)
            myGdxGame = new Solo(this);
        MyBackground.instance().setX(0);
        noPub = null;
        analytics.screenName("MyGdxGame");
        setScreen(myGdxGame);
    }


    public void goToHome() {
        if (home == null)
            home = new Home(kyfo);
        analytics.screenName("Home");
        requestManager.checkEmail();
        MyBackground.instance().setX(-1);
        requestManager.checkNbShare();
        setScreen(home);
    }

    public void logout() {
        Statistic.instance().reset();
        statistic.save();
    }

    public void goToOffline() {
        Statistic.instance().pub = true;
        goToMyGdxGame();
    }


    public void goToNoPub() {
        if (noPub == null)
            noPub = new NoPub(this);
        analytics.screenName("NoPub");
        MyBackground.instance().setX(0);
        setScreen(noPub);
    }

    public void showInterstitial() {
        pub.showInterstitial();
    }

    public IAnalytics   getAnalytics() {
        return analytics;
    }

    @Override
    public void render() {
        super.render();
    }
}
