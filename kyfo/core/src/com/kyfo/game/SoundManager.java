package com.kyfo.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by lambe_000 on 27/01/2015.
 */
public class SoundManager {
    HashMap<String, Sound>  sounds;

    private SoundManager() {
        sounds = null;
    }

    public void load() {
        if (sounds == null) {
            sounds = new HashMap<String, Sound>();
            FileHandle[] files = Gdx.files.internal("sound/effect/").list();
            for(FileHandle file: files) {
                Sound sound = Gdx.audio.newSound(Gdx.files.internal(file.path()));
                sounds.put(file.nameWithoutExtension(), sound);
            }
        }
    }

    public Sound getSound(String name) {
        return sounds.get(name);
    }

    public static SoundManager instance() {
        return SingletonHolder.instance;
    }

    private static class SingletonHolder
    {
        private final static SoundManager instance = new SoundManager();
    }
}
