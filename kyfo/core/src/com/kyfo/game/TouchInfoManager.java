package com.kyfo.game;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by lambe_000 on 19/05/2015.
 */
public class TouchInfoManager {
    TouchInfo               touchA, touchB;
    boolean permutation;

    public static TouchInfoManager instance() {
        return SingletonHolder.instance;
    }

    private static class SingletonHolder
    {
        private final static TouchInfoManager instance = new TouchInfoManager();
    }

    private TouchInfoManager() {
        permutation = true;
        touchA = new TouchInfo();
        touchB = new TouchInfo();
    }

    public boolean noInvincible() {
        return (!touchA.killed && !touchB.killed);
    }

    public Vector2 getDirectionAB() {
        if (!permutation) {
            return touchA.position().sub(touchB.position());
        } else {
            return touchB.position().sub(touchA.position());
        }
    }

    public TouchInfo getTouchCenter() {
        return permutation ? touchA : touchB;
    }

    public boolean nothingPointerTouched() {
        return !(touchA.touched);
    }
}
