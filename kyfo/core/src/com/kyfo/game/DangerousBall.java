package com.kyfo.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by lambe_000 on 19/05/2015.
 */
class   DangerousBall extends Ball {

    public DangerousBall(Vector2 position, Vector2 direction, float rayon, float vitesse) {
        super(Color.BLACK, position, direction, rayon, vitesse);
        Texture t = TextureManager.instance().getTexture("boule");
        sprite = new Sprite(t);
        sprite.setScale(1 / 312f * rayon);
    }

    public void destroy() {
        gameManager.win(new Vector2(getPosition()));
        gameManager.nbDestroy++;
        if (gameManager.nbDestroy > gameManager.mult * 10) {
            gameManager.nbDestroy = 0;
            Ball ball = new MultBall(ballManager.randomPosition(), new Vector2(direction), UNITY / 1.5f, 1);
            ball.followPlayer();
            ballManager.add(ball);
        }
    }

    public void render(SpriteBatch spriteBatch) {
        super.render(spriteBatch);
        if (follow)
            followPlayer();
    }

    public void effect() {
        if (ghost || touchManager.getTouchCenter().endTouch || touchManager.getTouchCenter().killed)
            return ;
        SoundManager.instance().getSound("impact").play();
        ballManager.remove(this);
        gameManager.hit();
    }
}