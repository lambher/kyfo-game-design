package com.kyfo.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by lambe_000 on 02/01/2015.
 */
public class Ressources {
    private MyFont  font;
    private Texture textureBouleNoire;
    private Texture textureBouleLife;
    private Texture textureBouleLifeEmpty;
    private Texture textureSoufle;
    private Texture textureGravity;
    private Texture textureClear;
    private Texture textureLvl;

    private Ressources() {
        font = null;
        textureBouleNoire = null;
        textureBouleLife = null;
        textureBouleLifeEmpty = null;
        textureSoufle = null;
        textureGravity = null;
        textureClear = null;
        textureLvl = null;
    }

    public static Ressources instance() {
        return SingletonHolder.instance;
    }

    private static class SingletonHolder
    {
        private final static Ressources instance = new Ressources();
    }

    public MyFont getFont(String path, int size) {
        return new MyFont("font/trans.ttf", size);
//        if (font == null) {
//            font = new MyFont("font/trans.ttf", size);
//        }
//        return font;
    }
}
