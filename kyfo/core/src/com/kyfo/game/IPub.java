package com.kyfo.game;

/**
 * Created by lambe_000 on 05/01/2015.
 */
public interface IPub {
    public void     showInterstitial();
    public void     showAdView();
    public boolean  isNetworkConnected();
    public void     dialogueShare(IConnection ok);
    public void     dialogueLogin(IConnection ok);
    public void     dialogueInscription(IConnection ok);
    public void     submitScore();
    public void     displayScore();
    public void     unlockTime60();
    public void     unlockTime120();
    public void     displayAchievement();
    public void     firstTime();
    public void     unlockFive();
    public void     scan(IConnection share);
    public void     getRegId(IAction action);
    public void     generateQRCode(IConnection action);
}
