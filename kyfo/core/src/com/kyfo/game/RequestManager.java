package com.kyfo.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpParametersUtils;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lambe_000 on 10/05/2015.
 */
public class RequestManager {

    Statistic statistic;
    public String url;

    private RequestManager() {
        statistic = Statistic.instance();
    }

    public void checkEmail() {
        if (statistic.email == null) {
            return ;
        }
        if (!statistic.email.equals("")) {
            Map parameters = new HashMap();
            parameters.put("email", statistic.email);
            Net.HttpRequest httpGet = new Net.HttpRequest(Net.HttpMethods.GET);
            httpGet.setUrl(url + "new");
            httpGet.setContent(HttpParametersUtils.convertHttpParameters(parameters));
            Gdx.net.sendHttpRequest (httpGet, new Net.HttpResponseListener() {
                public void handleHttpResponse(Net.HttpResponse httpResponse) {
                    String status = httpResponse.getResultAsString();

                    JsonReader jsonReader = new JsonReader();

                    JsonValue reponse = jsonReader.parse(status);
                    if (reponse.getBoolean("result")) {
                    }
                }

                public void failed(Throwable t) {
                    String status = "error";
                    //do stuff here based on the failed attempt
                }

                @Override
                public void cancelled() {

                }
            });
        } else {
        }

    }

    public void sendRegId() {
        Map parameters = new HashMap();
        parameters.put("email", statistic.email);
        parameters.put("reg_id", statistic.registration_ID);
        Net.HttpRequest httpGet = new Net.HttpRequest(Net.HttpMethods.GET);
        httpGet.setUrl(url + "reg_id");
        httpGet.setContent(HttpParametersUtils.convertHttpParameters(parameters));
        Gdx.net.sendHttpRequest (httpGet, new Net.HttpResponseListener() {
            public void handleHttpResponse(Net.HttpResponse httpResponse) {

            }

            public void failed(Throwable t) {
                String status = "error";
                //do stuff here based on the failed attempt
            }

            @Override
            public void cancelled() {

            }
        });
    }


    public void checkNbShare() {
        if (statistic.email.equals(""))
            return ;
        Map parameters = new HashMap();
        parameters.put("email", statistic.email);
        Net.HttpRequest httpGet = new Net.HttpRequest(Net.HttpMethods.GET);
        httpGet.setUrl(url + "nbShare");
        httpGet.setContent(HttpParametersUtils.convertHttpParameters(parameters));
        Gdx.net.sendHttpRequest (httpGet, new Net.HttpResponseListener() {
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                String status = httpResponse.getResultAsString();

                JsonReader jsonReader = new JsonReader();

                JsonValue reponse = jsonReader.parse(status);
                if (reponse.getBoolean("result")) {
                    statistic.shared = reponse.getBoolean("shared");
                    statistic.nbShare = reponse.getInt("nbShare");
                    if (statistic.nbShare >= 3)
                        statistic.pub = false;
                    else
                        statistic.pub = true;
                    statistic.save();
                }
            }

            public void failed(Throwable t) {
                String status = "error";
                //do stuff here based on the failed attempt
            }

            @Override
            public void cancelled() {

            }
        });
    }


    public static RequestManager instance() {
        return SingletonHolder.instance;
    }

    private static class SingletonHolder
    {
        private final static RequestManager instance = new RequestManager();
    }
}
