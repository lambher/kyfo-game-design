package com.kyfo.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lambe_000 on 19/05/2015.
 */
public class UIManager {
    private GameManager             gameManager = GameManager.instance();
    private static UIManager        ourInstance = new UIManager();
    private List<BonusPoint>        bonusPoints = new ArrayList<BonusPoint>();
    public static UIManager         instance() {
        return ourInstance;
    }
    private Statistic               statistic = Statistic.instance();
    private int                     width, height, UNITY;
    private Vector2                 totalPosition;
    private float                   scale;
    private Color                   colorGUI;
    private Sprite                  sLife;
    private Sprite                  sLifeEmpty;
    private float                   transparency;

    private UIManager() {
        totalPosition = new Vector2(0, 0);
        width = Gdx.graphics.getWidth();
        height = Gdx.graphics.getHeight();
        UNITY = statistic.UNITY;
        scale = 0.1f;
        transparency = 0f;
        colorGUI = new Color();
        sLife = new Sprite(TextureManager.instance().getTexture("life"));
        sLifeEmpty = new Sprite(TextureManager.instance().getTexture("lifeEmpty"));
        sLife.setScale(1 / 312f * UNITY / 1.5f);
        sLifeEmpty.setScale(1 / 312f * UNITY / 1.5f);
    }

    public void bonus(Vector2 position, int score) {
        bonusPoints.add(new BonusPoint(new Vector2(position), score));
    }

    public void render(OrthographicCamera camera, MyFont font, SpriteBatch batch, ShapeRenderer shapeRenderer, float opacity) {
        font.draw(batch, Color.GREEN, gameManager.message, width / 2 - 5 * UNITY, scale);
        font.draw(batch, colorGUI,"Mult", UNITY, height - UNITY * 3);
        font.draw(batch, colorGUI,"" + gameManager.mult, UNITY * 4, height - UNITY * 3);
        font.draw(batch, colorGUI,"Score", totalPosition.x + UNITY, -totalPosition.y + height - UNITY);
        font.draw(batch, colorGUI, "" + gameManager.score, totalPosition.x + UNITY * 4, -totalPosition.y + height - UNITY);
        font.draw(batch, colorGUI,"Best", UNITY, height - UNITY * 2);
        font.draw(batch, colorGUI,"" + statistic.maxTotal, UNITY * 4, height - UNITY * 2);
        for (int i = 0; i < bonusPoints.size(); i++) {
            BonusPoint bonusPoint = bonusPoints.get(i);
            bonusPoint.render(font, batch);
            if (!bonusPoint.show) {
                bonusPoints.remove(i);
                i--;
            }
        }
        batch.end();

        for (int i = 0; i < gameManager.max_life; i++) {
            batch.begin();
            if (gameManager.life > i) {
                sLife.setAlpha(opacity);
                sLife.setPosition(UNITY + (i * UNITY / 0.75f) + UNITY / 1.5f - 256, height - UNITY * 5 + UNITY / 2f - 256); // -7 -19
                sLife.draw(batch);
            } else {
                sLifeEmpty.setAlpha(opacity);
                sLifeEmpty.setPosition(UNITY + (i * UNITY / 0.75f) + UNITY / 1.5f - 256 , height - UNITY * 5 + UNITY / 2f - 256);
                sLifeEmpty.draw(batch);
            }
            batch.end();
        }

        colorGUI.set(0, 1, 0, opacity);



        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(new Color(0, 0, 0, transparency));
        shapeRenderer.rect(0, 0, width, height);
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    public void update() {
        gameManager = GameManager.instance();
        if (!gameManager.begin) {
            if (scale < height / 1.8f) {
                scale += (height / 1.8f - scale) / 10;
            }
        } else if(scale < height * 1.5) {
            scale += 100;
        }
        if (gameManager.game_slaw) {
            transparency = 0.25f;
        } else {
            transparency = 0;
        }
    }

    public void resume() {
            scale = 0;
    }
}
