package com.kyfo.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.net.HttpParametersUtils;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lambe_000 on 08/01/2015.
 */
public class NoPub implements Screen, InputProcessor {
    private SpriteBatch batch;

    Kyfo game;
    ShapeRenderer shapeRenderer;
    int width, height;
    Map<Integer, TouchInfo> touches = new HashMap<Integer, TouchInfo>();
    Map<Integer, Button> buttons = new HashMap<Integer, Button>();
    private Toast toast;
    Ressources ressources;
    Statistic statistic;
    int UNITY = Statistic.instance().UNITY;
    private MyFont font;
    private Pixmap pmap;
    private Texture tex;
    private Sprite face;

    public NoPub(final Kyfo game) {
        toast = Toast.instance();

        this.game = game;
        face = null;
        game.pub.generateQRCode(new IConnection() {
            @Override
            public void generate(byte[] bytes) {
                onByteArrayOfCroppedImageReciever(bytes);
            }

            @Override
            public void share(String mail_cible) {

            }

            @Override
            public void connect(String login, String password) {

            }

            @Override
            public void create(String login, String password, String sponsor) {

            }
        });
        statistic = Statistic.instance();
        ressources = Ressources.instance();
        font = ressources.getFont("font/HelveticaNeue.ttf", UNITY);
        width = Gdx.graphics.getWidth();
        height = Gdx.graphics.getHeight();
        shapeRenderer = new ShapeRenderer();
        batch = new SpriteBatch();

        for (int i = 0; i < 5; i++) {
            touches.put(i, new TouchInfo());
        }

    }

    class Button {
        boolean selected = false;
        String text;
        Vector2 position;
        IAction action;

        private Color color;

        public Button(String text, Vector2 position, IAction action) {
            color = Color.GREEN;
            this.text = text;
            this.position = position;
            this.action = action;
        }

        public void render() {
            font.draw(batch, color, text, position.x, height - position.y);
        }

        public void exec() {
            action.exec();
        }

        public void select(TouchInfo touchInfo) {
            for (int i = 0; i < buttons.size(); i++) {
                if (buttons.get(i).selected)
                    return;
            }
            touchInfo.selected = this;
            selected = true;
            color = Color.GRAY;
        }

        public void unselect(TouchInfo touchInfo) {
            selected = false;
            color = Color.GREEN;
            touchInfo.selected = null;
        }
    }

    class TouchInfo {
        float valueIncrease = 1;
        float touchX = 0;
        float touchY = 0;
        boolean touched = false;
        boolean endTouch = false;
        float time = 0;
        boolean killed = false;
        float timeKilled = 0;
        Button selected = null;

        public void beginTouch() {
            if (!endTouch) {
                touched = true;
            }
            endTouch = false;
            this.time = 0;
            this.killed = false;
            this.timeKilled = 0;
        }

        public void endTouch() {
            endTouch = true;

        }

        public void kill() {
            killed = true;
            timeKilled = 0;

        }

        public void plop() {
            touched = false;
            endTouch = false;
            if (selected != null) {
                selected.exec();
                selected.unselect(this);
            }
            //remove(point);
        }

        public void render() {
            if (touched) {
               /* shapeRenderer.setColor(Color.BLACK);
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.circle(touchX, height - touchY, UNITY / 8);
                shapeRenderer.end();*/

                if (selected != null) {
                    if (!(touchY > selected.position.y - UNITY && touchY < selected.position.y + UNITY)) {
                        selected.unselect(this);
                    }
                }

                for (int i = 0; i < buttons.size(); i++) {
                    Button button = buttons.get(i);

                    if (touchY > button.position.y - UNITY && touchY < button.position.y + UNITY) {
                        button.select(this);

                    }
                }
            }


            if (endTouch) {
                this.time += Gdx.graphics.getDeltaTime();
                if (this.time > 0.1) {
                    this.time = 0;
                    plop();
                }
                /*if (nothingPointerTouched()) {
                    begin = false;
                    endGame();

                }*/
            }
        }
    }

    @Override
    public void dispose() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.19f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        MyBackground.instance().render(batch);
        for (int i = 0; i < buttons.size(); i++) {
            Button button = buttons.get(i);
            button.render();
        }
        if (face != null) {
            face.draw(batch);
        }
        font.draw(batch, Color.WHITE, "Share", UNITY * 4, height * .30f);
        font.draw(batch, Color.WHITE, "" + statistic.nbShare,  UNITY * 4 + UNITY * 4, height * .30f);
        font.draw(batch, Color.WHITE, "/3", UNITY * 4 + UNITY * 4.5f, height * .30f);
        font.draw(batch, Color.WHITE, "No Pub",  UNITY * 4, height * .40f);
        font.draw(batch, statistic.pub ? Color.RED : Color.GREEN, statistic.pub ? "OFF" : "ON",  UNITY * 4 + UNITY * 4, height * .40f);
        //fontEmail.render(batch, statistic.shared ? Color.RED : Color.GREEN, statistic.email, UNITY, height - UNITY * 1);
       // font.render(batch, Color.YELLOW, "$ " + statistic.money, UNITY, UNITY * 1);
        batch.end();
        for (int i = 0; i < 5; i++) {
            TouchInfo toucheinfo = touches.get(i);
            toucheinfo.render();
        }
    }

    @Override
    public void resize(int arg0, int arg1) {
    }

    @Override
    public void show() {
//        refresh();
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void hide() {

    }


    @Override
    public void resume() {
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.BACK) {
            SoundManager.instance().getSound("back").play();
            game.goToHome();
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (pointer < 1) {
            touches.get(pointer).touchX = screenX;
            touches.get(pointer).touchY = screenY;
            touches.get(pointer).beginTouch();
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (pointer < 1) {
            if (!touches.get(pointer).touched) {
                return false;
            }
            touches.get(pointer).touchX = screenX;
            touches.get(pointer).touchY = screenY;
            touches.get(pointer).endTouch();
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (pointer < 1) {
            touches.get(pointer).touchX = screenX;
            touches.get(pointer).touchY = screenY;
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public void onByteArrayOfCroppedImageReciever(byte[] bytes) {
        try {
            pmap=new Pixmap(bytes, 0, bytes.length);
            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    tex=new Texture(pmap);
                    face=new Sprite(tex);
                    face.setPosition(width / 2 - statistic.UNITY * 4, height - statistic.UNITY * 10);
                }
            });
        } catch(Exception e) {
            Gdx.app.log("KS", e.toString());
            e.printStackTrace();
        }
    }
}