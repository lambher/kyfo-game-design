package com.kyfo.game;

/**
 * Created by lambe_000 on 10/05/2015.
 */
public interface ShowToast {
    public void show(String message, boolean isShort);
}
