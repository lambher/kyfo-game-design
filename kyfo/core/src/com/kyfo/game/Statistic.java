package com.kyfo.game;

import com.badlogic.gdx.utils.JsonValue;

/**
 * Created by lambe_000 on 02/01/2015.
 */
public class Statistic {
    public boolean      New;
    public String       registration_ID;
    public int          maxTotal;
    public int          money;
    public String       pk;
    public String       login;
    public boolean      pub;
    public int          nbShare;
    public boolean      shared;
    public int          nbPlay;
    public float        time;
    public float        bestTime;
    public int          slow;
    public int          UNITY;
    public boolean      life, soufle, gravity, mult, clear;
    public String       email;
    private SaveStat    saveStat;

    private Statistic() {
        reset();
    }

    public void reset() {
        New = true;
        registration_ID = "reg_id";
        email = "";
        maxTotal = 0;
        shared = false;
        money = 0;
        slow = 100;
        nbPlay = 0;
        pk = "pk";
        bestTime = 0;
        login = "login";
        time = 0;
        pub = true;
        nbShare = 0;
        life = true;
        soufle = true;
        gravity = true;
        mult = true;
        clear = true;
        saveStat = null;
    }

    public void initialise(SaveStat saveStat) {
        this.saveStat = saveStat;
    }

    public void save() {
        if (saveStat != null)
            saveStat.save();
    }

    public static Statistic instance() {
        return SingletonHolder.instance;
    }

    private static class SingletonHolder
    {
        private final static Statistic instance = new Statistic();
    }
}
