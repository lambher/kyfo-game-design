package com.kyfo.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

/**
 * Created by lambe_000 on 19/05/2015.
 */
public class KyfoInput implements InputProcessor {

    private Kyfo    game;

    public KyfoInput(Kyfo game) {
        this.game = game;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        GameManager gameManager = GameManager.instance();
        TouchInfoManager touchManager = TouchInfoManager.instance();
        BallManager ballManager = BallManager.instance();
        UIManager uiManager = UIManager.instance();

        if(keycode == Input.Keys.BACK){
            game.goToHome();
            ballManager.clearAll();
            ballManager.fast();
            gameManager.mult = 1;
            MusicManager.instance().stop();
            uiManager.resume();
            if (gameManager.begin)
                SoundManager.instance().getSound("lost").play();
            else
                SoundManager.instance().getSound("back").play();
            gameManager.begin = false;
            gameManager.life = 0;
            touchManager.touchA.touched = false;
            touchManager.touchB.touched = false;
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        GameManager gameManager = GameManager.instance();
        TouchInfoManager touchManager = TouchInfoManager.instance();
        BallManager ballManager = BallManager.instance();

        if (pointer < 2) {
            TouchInfo touchInfo = pointer == 0 ? touchManager.touchA : touchManager.touchB;

            if (pointer == 0) {
                ballManager.fast();
                if (touchInfo.goTouchX < 0)
                    touchInfo.touchX = screenX;
                else
                    touchInfo.goTouchX = screenX;
                if (touchInfo.goTouchY < 0)
                    touchInfo.touchY = screenY - 200 * Gdx.graphics.getHeight() / 1920;
                else
                    touchInfo.goTouchY = screenY - 200 * Gdx.graphics.getHeight() / 1920;
            } else {
                touchInfo.touchX = screenX;
                touchInfo.touchY = screenY;
            }

            if (touchInfo.touchY <= 0) {
                touchInfo.touchY = 1;
            }
            touchInfo.beginTouch();
        }
        if (!gameManager.begin) {
            if (touchManager.getTouchCenter().touched)
                gameManager.startGame();
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        TouchInfoManager touchManager = TouchInfoManager.instance();
        BallManager ballManager = BallManager.instance();

        if (pointer < 2) {

            TouchInfo touchInfo = pointer == 0 ? touchManager.touchA : touchManager.touchB;
            if (!touchInfo.touched) {
                return false;
            }
            if (touchInfo.touchY <= 0) {
                touchInfo.touchY = 1;
            }

            if (pointer == 1) {
                touchManager.touchB.touched = false;
            } else {
                // ballManager.slaw();
            }
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        TouchInfoManager touchManager = TouchInfoManager.instance();

        if (pointer < 2) {
            TouchInfo touchInfo = pointer == 0 ? touchManager.touchA : touchManager.touchB;
            if (pointer == 0) {
                touchInfo.goTouchX = screenX;
                touchInfo.goTouchY = screenY - 200 * Gdx.graphics.getHeight() / 1920;
            } else {
                touchInfo.touchX = screenX;
                touchInfo.touchY = screenY;
            }
            if (touchInfo.touchY <= 0) {
                touchInfo.touchY = 1;
            }
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
