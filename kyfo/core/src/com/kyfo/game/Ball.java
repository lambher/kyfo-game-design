package com.kyfo.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by lambe_000 on 19/05/2015.
 */
abstract class Ball {
    Sprite sprite;
    protected BallManager ballManager;
    float rotation;
    private Vector2 position;
    TouchInfo cible = null;
    boolean follow = true;
    Vector2 direction;
    Color color;
    boolean ghost = false;
    private int height;
    boolean slaw = false;
    float rayon, speed;
    GameManager gameManager = GameManager.instance();
    TouchInfoManager touchManager = TouchInfoManager.instance();
    Statistic statistic = Statistic.instance();
    int UNITY = statistic.UNITY;

    public Ball(Color color, Vector2 position, Vector2 direction, float rayon, float speed) {
        ballManager = BallManager.instance();
        this.slaw = false;
        height = Gdx.graphics.getHeight();
        this.color = color;
        this.rotation = 0;
        this.position = position;
//            this.position = getPosition();
        this.direction = direction;
        this.rayon = rayon;
        this.speed = 5 + (speed);
        this.sprite = null;
        if (gameManager.game_slaw) {
            slaw = true;
            this.speed /= gameManager.time_slaw;
        }
    }

    public void followPlayer() {
        TouchInfo touchInfo = touchManager.getTouchCenter();

        direction.x = touchInfo.touchX - position.x + MyBackground.instance().decal;
        direction.y = (height - touchInfo.touchY) - position.y;
    }

    public void fast() {
        if (slaw) {
            speed *= gameManager.time_slaw;
            slaw = false;
        }
    }

    public void slaw() {
        if (!slaw) {
            speed /= gameManager.time_slaw;
            slaw = true;
        }
    }

    public boolean colision(TouchInfo touchInfo) {
        return !(new Vector2(touchInfo.touchX, height - touchInfo.touchY).dst(getPosition()) > touchInfo.rayon + rayon);
    }

    public boolean colision(Bullet bullet) {
        return !(bullet.position.dst(getPosition()) > UNITY / 8 + rayon);
    }

    public abstract void effect();

    public Vector2 getPosition() {
        Vector2 p = new Vector2(position);
        p.x -= MyBackground.instance().decal;

        return p;
    }

    public void destroy() {
        effect();
    }

    public void render(SpriteBatch batch) {
        double k = 1 / Math.sqrt(direction.x * direction.x + direction.y * direction.y);

        if (cible != null) {
            if (cible.touched) {
                direction.x = cible.touchX - position.x;
                direction.y = (height - cible.touchY) - position.y;
            } else {
                cible = null;
            }
        }

        position.x += direction.x * k * UNITY / 100 * speed;
        position.y += direction.y * k * UNITY / 100 * speed;
        if (sprite != null) {
            sprite.setPosition(-MyBackground.instance().decal + position.x - 256, position.y - 256);
            sprite.setRotation(rotation);
            batch.begin();
            sprite.draw(batch);
            batch.end();
        }

    }
}