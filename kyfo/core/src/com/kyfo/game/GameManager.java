package com.kyfo.game;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by lambe_000 on 19/05/2015.
 */
public class GameManager {
    private static GameManager  ourInstance = new GameManager();
    private BallManager         ballManager = BallManager.instance();
    private TouchInfoManager    touchManager = TouchInfoManager.instance();
    private Statistic           statistic = Statistic.instance();
    private UIManager           uiManager = UIManager.instance();
    private Kyfo                game;
    public boolean              game_slaw;
    public float                time_slaw;
    public int                  life;
    public int                  max_life;
    public boolean              begin;
    public float                time;
    public int                  mult;
    public int                  score;
    public String               message;
    public int                  nbDestroy;

    public static GameManager instance() {
        return ourInstance;
    }

    private GameManager() {
        game = null;
        message = "Keep Your Fingers On";
        time = 1;
        life = 0;
        game_slaw = false;
        time_slaw = 5;
        max_life = 3;
        begin = false;
        nbDestroy = 0;
    }

    public void setGame(Kyfo game) {
        this.game = game;
    }

    public void addLife() {
        if (life < max_life) {
            life++;
        }
    }

    public void addMult() {
        mult++;
    }

    public void pub() {
        if (statistic.pub) {
            switch (MathUtils.random(1, 3)) {
                case 2:
                    if (game != null) {
                        game.showInterstitial();
                    }
                    break;
            }
        }
    }

    public void endGame() {
        ballManager = BallManager.instance();
        MusicManager.instance().stop();
        SoundManager.instance().getSound("lost").play();
        pub();
        statistic.save();
        TouchInfo touchInfo = touchManager.getTouchCenter();
        ballManager.fuite(new Vector2(touchInfo.touchX + MyBackground.instance().decal, touchInfo.touchY));
        begin = false;
        uiManager.resume();
        life = 0;
        touchManager.touchA.touched = false;
        touchManager.touchB.touched = false;
    }

    public void startGame() {
        SoundManager.instance().getSound("play").play();
        MusicManager.instance().random();
        MusicManager.instance().play();
        statistic.nbPlay++;
        statistic.time = 0;
        time = 1;
        mult = 1;
        score = 0;
        statistic.time = 0;
        statistic.slow = 10;
        begin = true;
        nbDestroy = 0;
    }

    public void hit() {
        life--;
        touchManager.getTouchCenter().hit();
        if (life >= 0)
            return;
        endGame();
    }

    public void win(Vector2 position) {
        uiManager.bonus(position, mult);
        score += mult;
        if (score > statistic.maxTotal)
            statistic.maxTotal = score;
        SoundManager.instance().getSound("coin").play();
    }
}
