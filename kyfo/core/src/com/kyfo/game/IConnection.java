package com.kyfo.game;

/**
 * Created by lambe_000 on 21/01/2015.
 */
public interface IConnection {
    public void generate(byte[] bytes);
    public void share(String ip_cible);
    public void connect(String login, String password);
    public void create(String login, String password, String sponsor);
}
