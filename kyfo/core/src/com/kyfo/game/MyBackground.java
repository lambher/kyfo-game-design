package com.kyfo.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by lambe_000 on 28/02/2015.
 */

public class MyBackground {
    public float decal;
    Sprite background;
    int height, width;
    float x, goX;
    private MyBackground() {
        background = new Sprite(TextureManager.instance().getTexture("background"));
        height = Gdx.graphics.getHeight();
        width = Gdx.graphics.getWidth();
        x = -1;
        goX = -1;
        decal = background.getWidth() / 2 - width / 2;
        decal *= x;
    }

    public void render(SpriteBatch batch) {
        float coef = ((float) height) / background.getHeight();
        decal = (coef * background.getWidth()) / 2 - width / 2;
        decal *= (x / 2);

        background.setPosition(-decal + width / 2 - background.getWidth() / 2, height / 2 - background.getHeight() / 2);
        background.setScale(coef, coef);
        background.draw(batch);

        x += (goX - x) / 20;
    }

    public void setX(float x) {
        goX = x;
    }

    public static MyBackground instance() {
        return SingletonHolder.instance;
    }

    private static class SingletonHolder
    {
        private final static MyBackground instance = new MyBackground();
    }
}
