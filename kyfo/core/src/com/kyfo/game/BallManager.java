package com.kyfo.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lambe_000 on 19/05/2015.
 */
public class BallManager {
    private List<Ball>          balls = new ArrayList<Ball>();
    private List<Bullet>        bullets = new ArrayList<Bullet>();
    private GameManager         gameManager = GameManager.instance();
    private TouchInfoManager    touchManager = TouchInfoManager.instance();
    private Statistic           statistic = Statistic.instance();
    private int                 width, height;
    private float               timeFire;
    private int                 MAXBOULLE, UNITY;

    public static BallManager instance() {
        return SingletonHolder.instance;
    }

    private static class SingletonHolder
    {
        private final static BallManager instance = new BallManager();
    }

    private BallManager() {
        width = Gdx.graphics.getWidth();
        height = Gdx.graphics.getHeight();
        timeFire = 0;
        MAXBOULLE = 40;
        UNITY = statistic.UNITY;
    }

    public void add(Ball ball) {
        balls.add(ball);
    }

    public void add(Bullet bullet) {
        bullets.add(bullet);
    }

    public void remove(Ball ball) {
        balls.remove(ball);
    }

    public void remove(Bullet bullet) {
        bullets.remove(bullet);
    }

    public void clearBall() {
        balls.clear();
    }

    public void clearBullet() {
        bullets.clear();
    }

    public void clearAll() {
        clearBall();
        clearBullet();
    }

    public int nbBall() {
        return balls.size();
    }

    public Ball getBall(int index) {
        return balls.get(index);
    }

    public void fuite(Vector2 cible) {
        for (int i = 0; i < balls.size(); i++) {
            Ball ball = balls.get(i);
            if (ball.color == Color.BLACK) {
                ball.direction.x = cible.x - ball.getPosition().x;
                ball.direction.y = (Gdx.graphics.getHeight() - cible.y) - ball.getPosition().y;
                ball.speed = 50;
                ball.follow = false;
                ball.ghost = true;
            }
        }
    }

    public void slaw() {
        gameManager.game_slaw = true;
        gameManager.time_slaw = 0;
        for (int i = 0; i < balls.size(); i++) {
            balls.get(i).slaw();
        }
        for (int i = 0; i < bullets.size(); i++) {
            bullets.get(i).slaw();
        }
    }

    public void fast() {
        if (gameManager.game_slaw) {
            for (int i = 0; i < balls.size(); i++) {
                balls.get(i).fast();
            }
            for (int i = 0; i < bullets.size(); i++) {
                bullets.get(i).fast();
            }
            gameManager.game_slaw = false;
        }
    }

    public void update() {
        this.timeFire += gameManager.game_slaw ? Gdx.graphics.getDeltaTime() / gameManager.time_slaw : Gdx.graphics.getDeltaTime();
        if (timeFire > 1 / (1+ 4 * Math.log(.0125f * (gameManager.score + statistic.time / 50f) + 1))) {
            timeFire = 0;
            fire();
        }
    }

    public Vector2 randomPosition() {
        Vector2 position = new Vector2();
        float decal = MyBackground.instance().decal;
        switch (MathUtils.random(0, 3)) {
            case 0:
                position = new Vector2(MathUtils.random(decal, width + decal), height);

                break;
            case 1:
                position = new Vector2(width + decal, MathUtils.random(0, height));
                break;
            case 2:
                position = new Vector2(decal, MathUtils.random(0, height));
                break;
            case 3:
                position = new Vector2(MathUtils.random(decal, width + decal), 0);
                break;
        }
        return position;
    }

    public Vector2 directionPlayer() {
        Vector2 direction = new Vector2();

        return direction;
    }

    public void fire() {
        if (balls.size() >= MAXBOULLE) {
            return ;
        }
        if (balls.size() >= MAXBOULLE) {
            return;
        }
        TouchInfo touchInfo = touchManager.getTouchCenter();

        if (touchInfo.touched) {
            Vector2 position = randomPosition();

            Vector2 directionNoire = new Vector2();
            Vector2 directionBonus = new Vector2();
            directionNoire.x = touchInfo.touchX - position.x + MyBackground.instance().decal;
            directionNoire.y = (height - touchInfo.touchY) - position.y;
            float speed = (float) (Math.log(statistic.time / 500f +1));
            float rayon = UNITY / 2;
            float speedBonus = speed / 2;
            switch (MathUtils.random(1, 8 / 1)) {
                case 1:
                    directionBonus.x = width / 2 - position.x;
                    directionBonus.y = height / 2 - position.y;
                    directionBonus = directionNoire;
                    switch (MathUtils.random(1, 4)) {
                        case 1:
                            add(statistic.clear ? new ClearBall(position, directionBonus, UNITY / 1.5f, speedBonus) : new DangerousBall(position, directionNoire, rayon, speed));
                            break;
                        case 2:
                            add(statistic.soufle ? new ShootBall(position, directionBonus, UNITY / 1.5f, speedBonus) : new DangerousBall(position, directionNoire, rayon, speed));
                            break;
                        case 3:
                            add(statistic.gravity ? new GravityBall(position, directionBonus, UNITY / 1.5f, speedBonus) : new DangerousBall(position, directionNoire, rayon, speed));
                            break;
                        case 4:
                            if (touchManager.noInvincible() && gameManager.life < gameManager.max_life) {
                                add(statistic.life ? new LifeBall(position, directionBonus, UNITY / 1.5f, speedBonus) : new DangerousBall(position, directionNoire, rayon, speed));
                            } else {
                                add(statistic.soufle ? new ShootBall(position, directionBonus, UNITY / 1.5f, speedBonus) : new DangerousBall(position, directionNoire, rayon, speed));
                            }
                    }
                    break;
                default:
                    Ball ball = new DangerousBall(position, directionNoire, rayon, speed);//(Totalpoint / 50) > 4 ? 0 : Totalpoint / 50);
                    add(ball);
            }
        }
    }

    public void render(ShapeRenderer shapeRenderer, SpriteBatch batch) {
        for (int i = 0; i < bullets.size(); i++) {
            Bullet bullet = bullets.get(i);
            bullet.draw(shapeRenderer);
            if (bullet.position.x < 0 - bullet.rayon || bullet.position.x > width + bullet.rayon || bullet.position.y < 0 - bullet.rayon || bullet.position.y > height + bullet.rayon) {
                //DELETE
                remove(bullet);
                i--;
            }
        }

        for (int i = 0; i < balls.size(); i++) {
            Ball ball = balls.get(i);
            ball.render(batch);
            if (ball.getPosition().x < 0 - ball.rayon || ball.getPosition().x > width + ball.rayon || ball.getPosition().y < 0 - ball.rayon || ball.getPosition().y > height + ball.rayon) {
                //DELETE
                remove(ball);
                i--;
            }
            boolean explosed = true;
            for (int u = 0; u < bullets.size() && explosed; u++) {
                Bullet bullet = bullets.get(u);
                if (ball.colision(bullet)) {
                    remove(ball);
                    remove(bullet);
                    u--;
                    i--;
                    explosed = false;
                    ball.destroy();
                }
            }
            for (int u = 0; u < 1; u++) {
                TouchInfo touchInfo = u == 0 ? touchManager.touchA : touchManager.touchB;
                if (touchInfo.touched) {
                    if (ball.colision(touchInfo)) {
                        ball.effect();
                    }
                }
            }


        }
    }
}
