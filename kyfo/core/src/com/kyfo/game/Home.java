package com.kyfo.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lambe_000 on 26/12/2014.
 */
public class Home implements Screen, InputProcessor {
    private SpriteBatch     batch ;
    Kyfo                    game;
    ShapeRenderer           shapeRenderer;
    int                     width, height;
    Map<Integer,TouchInfo>  touches = new HashMap<Integer, TouchInfo>();
    Map<Integer,Button>     buttons = new HashMap<Integer, Button>();
    Ressources              ressources;
    Statistic               statistic;
    int                     UNITY = Statistic.instance().UNITY;
    private MyFont          font;


    public Home(final Kyfo game) {
        this.game = game;
        statistic = Statistic.instance();
        ressources = Ressources.instance();
        font = ressources.getFont("font/HelveticaNeue.ttf", UNITY);
        width = Gdx.graphics.getWidth();
        height = Gdx.graphics.getHeight();

        buttons.put(0, new Button("Play", new Vector2(width / 2 - 2 * UNITY, height * .25f), new IAction() {
            @Override
            public void exec() {
                SoundManager.instance().getSound("button").play();
                game.getAnalytics().event("HomeButton", "click", "Play");
                game.goToMyGdxGame();
            }
        }));
        buttons.put(1, new Button("Rank", new Vector2(width / 2 - 2 * UNITY, height * .42f), new IAction() {
            @Override
            public void exec() {
                SoundManager.instance().getSound("button").play();
                game.getAnalytics().event("HomeButton", "click", "Rank");
                game.displayScore();
            }
        }));
        buttons.put(3, new Button("No Pub", new Vector2(width / 2 - 2 * UNITY, height * .59f), new IAction() {
            @Override
            public void exec() {
                SoundManager.instance().getSound("button").play();
                game.getAnalytics().event("HomeButton", "click", "No Pub");
                game.goToNoPub();
            }
        }));
        buttons.put(2, new Button("Achievement", new Vector2(width / 2 - 2 * UNITY, height * .76f), new IAction() {
            @Override
            public void exec() {
                SoundManager.instance().getSound("button").play();
                game.getAnalytics().event("HomeButton", "click", "Achievement");
                game.pub.displayAchievement();
            }
        }));

        shapeRenderer = new ShapeRenderer();
        batch = new SpriteBatch();

        for (int i = 0; i < 5; i++) {
            touches.put(i, new TouchInfo());
        }
    }

    class Button {
        boolean selected = false;
        String  text;
        Vector2 position;
        IAction action;

        private Color   color;

        public Button(String text, Vector2 position, IAction action) {
            color = Color.GREEN;
            this.text = text;
            this.position = position;
            this.action = action;
        }

        public void render() {
            font.draw(batch, color, text, position.x, height - position.y);
        }

        public void exec() {

            action.exec();
        }

        public void select(TouchInfo touchInfo) {
            for (int i = 0; i < buttons.size(); i++) {
                if (buttons.get(i).selected)
                    return ;
            }
            touchInfo.selected = this;
            selected = true;
            color = Color.GRAY;
        }

        public void unselect(TouchInfo touchInfo) {
            selected = false;
            color = Color.GREEN;
            touchInfo.selected = null;
        }
    }

    class TouchInfo {
        float   valueIncrease = 1;
        float   touchX = 0;
        float   touchY = 0;
        boolean touched = false;
        boolean endTouch = false;
        float   time = 0;
        boolean killed = false;
        float   timeKilled = 0;
        Button  selected = null;

        public void beginTouch() {
            if (!endTouch) {
                touched = true;
            }
            endTouch = false;
            this.time = 0;
            this.killed = false;
            this.timeKilled = 0;
        }

        public void endTouch() {
            endTouch = true;

        }

        public void kill() {
            killed = true;
            timeKilled = 0;

        }

        public void plop() {
            touched = false;
            endTouch = false;
            if (selected != null) {
                selected.exec();
                selected.unselect(this);
            }
            //remove(point);
        }

        public void render() {
            if (touched) {
               /* shapeRenderer.setColor(Color.BLACK);
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.circle(touchX, height - touchY, UNITY / 8);
                shapeRenderer.end();*/

                if (selected != null) {
                    if (!(touchY > selected.position.y - UNITY && touchY < selected.position.y + UNITY)) {
                        selected.unselect(this);
                    }
                }

                for (int i = 0; i < buttons.size(); i++) {
                    Button button = buttons.get(i);

                    if (touchY > button.position.y - UNITY && touchY < button.position.y + UNITY) {
                        button.select(this);

                    }
                }
            }



            if (endTouch) {
                this.time += Gdx.graphics.getDeltaTime();
                if (this.time > 0.1) {
                    this.time = 0;
                    plop();
                }
                /*if (nothingPointerTouched()) {
                    begin = false;
                    endGame();

                }*/
            }
        }
    }

    @Override
    public void dispose() {
    }
    @Override
    public void pause() {
    }
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.19f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        MyBackground.instance().render(batch);
        for (int i = 0; i < buttons.size(); i++) {
            Button button = buttons.get(i);
            button.render();
        }
       // fontEmail.render(batch, statistic.shared ? Color.RED : Color.GREEN, statistic.email, UNITY, height - UNITY * 1);
       // font.render(batch, Color.YELLOW, "$ " + statistic.money, UNITY, UNITY * 1);
        batch.end();
        for (int i = 0; i < 5; i++) {
            TouchInfo toucheinfo = touches.get(i);
            toucheinfo.render();
        }

    }

    @Override
    public void resize(int arg0, int arg1) {
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void hide() {

    }


    @Override
    public void resume() {
    }

    @Override
    public boolean keyDown(int keycode) {
        if(keycode == Input.Keys.BACK){

        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (pointer < 1) {
            touches.get(pointer).touchX = screenX;
            touches.get(pointer).touchY = screenY;
            touches.get(pointer).beginTouch();
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (pointer < 1) {
            if (!touches.get(pointer).touched) {
                return false;
            }
            touches.get(pointer).touchX = screenX;
            touches.get(pointer).touchY = screenY;
            touches.get(pointer).endTouch();
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (pointer < 1) {
            touches.get(pointer).touchX = screenX;
            touches.get(pointer).touchY = screenY;
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

}