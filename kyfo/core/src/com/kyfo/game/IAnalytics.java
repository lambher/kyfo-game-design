package com.kyfo.game;

/**
 * Created by lambe_000 on 11/01/2015.
 */
public interface IAnalytics {
    public void screenName(String screenName);
    public void event(String categoryId, String actionId, String labelId);
}
