package com.kyfo.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by lambe_000 on 19/05/2015.
 */
class   LifeBall extends Ball {
    public LifeBall(Vector2 position, Vector2 direction, float rayon, float vitesse) {
        super(Color.RED, position, direction, rayon, vitesse);
        sprite = new Sprite(TextureManager.instance().getTexture("life"));
        sprite.setScale(1 / 312f * rayon);
    }

    public void effect() {
        SoundManager.instance().getSound("life").play();
        gameManager.addLife();
        ballManager.remove(this);
    }
}