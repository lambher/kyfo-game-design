package com.kyfo.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;

import java.util.HashMap;

/**
 * Created by lambe_000 on 29/01/2015.
 */
public class TextureManager {

    HashMap<String, Texture> textures;

    private TextureManager() {
        textures = null;
    }

    public void load() {
        if (textures == null) {
            textures = new HashMap<String, Texture>();
            FileHandle[] files = Gdx.files.internal("texture/").list();
            for(FileHandle file: files) {
                Texture texture =  new Texture(Gdx.files.internal(file.path()));
                textures.put(file.nameWithoutExtension(), texture);
            }
        }
    }

    public Texture getTexture(String name) {
        return textures.get(name);
    }

    public static TextureManager instance() {
        return SingletonHolder.instance;
    }

    private static class SingletonHolder
    {
        private final static TextureManager instance = new TextureManager();
    }
}
