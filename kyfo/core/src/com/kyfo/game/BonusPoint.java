package com.kyfo.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by lambe_000 on 25/04/2015.
 */
public class BonusPoint {
    private int     UNITY = Statistic.instance().UNITY;
    private int     point;
    private Vector2 position;
    private float   scale = 0;
    public boolean  show = false;

    public BonusPoint(Vector2 position, int point) {
        this.position = position;
        this.point = point;
        scale = 0;
        show = true;
    }

    public void render(MyFont font, SpriteBatch batch) {
        if (show) {
            if (position.y + scale + 1 < position.y + UNITY) {
                scale += (position.y + UNITY - (position.y + scale)) / 10;
            } else {
                show = false;
            }
            font.draw(batch, Color.GREEN, "+" + point, position.x, position.y + scale);
        }
    }


}
