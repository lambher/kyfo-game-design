package com.kyfo.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by lambe_000 on 19/05/2015.
 */
public class Bullet {
    public Vector2 position;
    public Vector2 direction;
    public float   speed;
    public float   rayon;
    boolean slaw = false;
    Statistic statistic = Statistic.instance();
    private Color color;
    private int     height, width;
    private GameManager gameManager = GameManager.instance();
    private int     UNITY = Statistic.instance().UNITY;

    public Bullet(Vector2 position, Vector2 direction, float speed) {
        this.slaw = false;
        this.position = position;
        this.direction = direction;
        this.speed = speed;
        this.color = Color.RED;

        height = Gdx.graphics.getHeight();
        width = Gdx.graphics.getWidth();

        if (gameManager.game_slaw) {
            slaw = true;
            this.speed /= gameManager.time_slaw;
        }

    }
    public void fast() {
        if (slaw) {
            speed *= gameManager.time_slaw;
            slaw = false;
        }
    }

    public void slaw() {
        if (!slaw) {
            speed /= gameManager.time_slaw;
            slaw = true;
        }
    }
    private void update() {
        double k = 1 / Math.sqrt(direction.x * direction.x + direction.y * direction.y);

        position.x += direction.x * k * UNITY / 100 * speed;
        position.y += direction.y * k * UNITY / 100 * speed;
    }

    public void draw(ShapeRenderer shapeRenderer) {
        update();

        shapeRenderer.setColor(color);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.circle(position.x, position.y, UNITY / 8);
        shapeRenderer.end();
    }
}