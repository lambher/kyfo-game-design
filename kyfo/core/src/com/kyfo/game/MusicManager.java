package com.kyfo.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.MathUtils;

import java.util.ArrayList;

/**
 * Created by lambe_000 on 27/01/2015.
 */
public class MusicManager {

    ArrayList<Music>    musics;
    int                 cursor;

    private MusicManager() {
        musics = null;
        cursor = 0;
    }

    public void load() {
        if (musics == null) {
            musics = new ArrayList<Music>();
            FileHandle[] files = Gdx.files.internal("sound/music/").list();
            for(FileHandle file: files) {
                Music music = Gdx.audio.newMusic(Gdx.files.internal(file.path()));
                music.setVolume(.5f);
                musics.add(music);
            }
        }
    }

    public void play() {
        musics.get(cursor).play();
    }

    public Music getMusic() {return musics.get(cursor);}

    public void stop() {
        musics.get(cursor).stop();
    }

    public void next() {
        cursor++;
        if (cursor >= musics.size()) {
            cursor = 0;
        }
    }

    public void random() {
        int tmp = cursor;
        //stop();
        cursor = MathUtils.random(0, musics.size() - 1);
        if (tmp == cursor)
            next();
        musics.get(cursor).setOnCompletionListener(new Music.OnCompletionListener() {
            @Override
            public void onCompletion(Music music) {
                random();
                play();
            }
        });
    }

    public static MusicManager instance() {
        return SingletonHolder.instance;
    }

    private static class SingletonHolder
    {
        private final static MusicManager instance = new MusicManager();
    }
}
